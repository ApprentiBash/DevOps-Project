#!/bin/bash

# Démarrez Vagrant
vagrant up

# Récupération de l'adresse IP de la machine Gitlab-serv via vagrant ssh-config
IP=$(vagrant ssh-config Flask-serv | grep HostName | awk '{print $2}')
echo "Adresse IP de Flask-serv: $IP"

# Génération de la paire de clés SSH si elle n'existe pas
if [ ! -f ~/.ssh/id_rsa ]; then
    echo "Génération d'une nouvelle paire de clés SSH..."
    ssh-keygen -t rsa -b 2048 -f ~/.ssh/id_rsa -N "" -q
fi

# Ajout de l'adresse IP du serveur aux hôtes connus
ssh-keyscan "$IP" >> ~/.ssh/known_hosts

# Copie de la clé publique sur la machine distante
echo "Copie de la clé publique vers $IP..."
ssh-copy-id -i ~/.ssh/id_rsa.pub root@"$IP"

# Tentative de connexion SSH sans mot de passe
echo "Test de la connexion SSH..."
ssh -i ~/.ssh/id_rsa -o BatchMode=yes -o ConnectTimeout=5 root@"$IP" 'echo "Connexion SSH réussie"'

# Vérification du résultat de la connexion
if [ $? -ne 0 ]; then
    echo "Échec de la connexion SSH. Veuillez vérifier la configuration SSH du serveur et les permissions des fichiers."
else
    echo "Connexion SSH réussie sans mot de passe."
fi
